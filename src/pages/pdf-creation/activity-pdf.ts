import { Component, ViewChildren, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, NavParams, Platform, Events } from 'ionic-angular';

// Ionic Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

// Services
import { Constants } from '../../providers/config/config';
import { HelperService } from '../../providers/helper-service/helper-service';
import { HelperPDFService } from '../../providers/helper-service/helper-pdf-service';
import { ImageService } from '../../providers/image-service/image-service';

// PDF Make
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

// Pipes
import { ReplacePipe } from '../../app/pipes/common.pipes';

@Component({
  templateUrl: 'activity-pdf.html'
})

export class ActivityPDFPage {
  // Document definition var for pdfMake
  dd: any;
  activity: any;
  pdfFileName: string = '';

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private events: Events,
    private platform: Platform,
    private helperService: HelperService,
    private helperPDFService: HelperPDFService,
    private ga: GoogleAnalytics,
    private file: File,
    private fileOpener: FileOpener,
    private imageService: ImageService,
    private replace: ReplacePipe,
  ) {
    this.activity = navParams.get('activity');
    this.ga.trackView('Create Activity PDF');
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    console.log(this.activity);
  }

  createPDFDocumentDefinition()
  {
    // Make PDF document definition object.
    var docContent = [];

    var reportTitle = this.activity.surveyAttempt.SurveyAttemptsTitle;
    this.pdfFileName = 'TNF Activity ' + reportTitle + '.pdf';

    var name = this.activity.surveyAttempt.User.UsersFirstName + ' ' + this.activity.surveyAttempt.User.UsersLastName;
    var date = this.activity.surveyAttempt.DateFormatted;

    // Location strings
    var countyString = '';
    if ( this.activity.location.Counties.length > 0 )
    {
      for ( var county in this.activity.location.Counties )
      {
        countyString += this.activity.location.Counties[county] + ', ';
      }
      countyString.slice(0, -3);
    }

    var districtString = '';
    if ( this.activity.location.Districts.length > 0 )
    {
      for ( var district in this.activity.location.Districts )
      {
        districtString += this.activity.location.Districts[district] + ', ';
      }
      districtString.slice(0, -3);
    }

    var activityString = '';
    for ( var activity in this.activity.activityTypes )
    {
      activityString += this.activity.activityTypes[activity] + '\n';
    }

    var audienceString = '';
    for ( var audience in this.activity.audienceTypes )
    {
      audienceString += this.activity.audienceTypes[audience] + '\n';
    }

    // Replace some things
    audienceString = this.replace.transform( audienceString, '<strong>', '' );
    audienceString = this.replace.transform( audienceString, '</strong>', '' );
    audienceString = this.replace.transform( audienceString, '&nbsp;', ' ' );

    var deliverableString = '';
    for ( var deliverable in this.activity.deliverables )
    {
      deliverableString += this.activity.deliverables[deliverable] + '\n';
    }

    // Header
    var dataHeader = {
      reportTitle: reportTitle,
      reportSubTitle: name + ', ' + date
    };
    docContent.push( this.helperPDFService.makePDFHeader( dataHeader ) );

    // Name, date, location table
    var activityDataHeader = [{
			columns: [
        {
          text: [
            { text: 'TA Mode ', bold: true },
            { text: this.activity.taMode + '\n' },
            { text: ' Duration ', bold: true },
            { text: this.activity.duration }
          ]
        },
        {
          text: [
            { text: this.activity.surveyAttempt.User.UsersRegion + '\n' },
            { text: 'Counties ', bold: true },
            { text: countyString + '\n' },
            { text: 'Districts ', bold: true },
            { text: districtString + '\n' },
          ]
        }
      ]
		}];
    docContent.push( activityDataHeader );

    // Comments
    var comments = [
      { text: '\n\nComments\n', style: 'tableHeader' },
      { text: this.activity.comments + '\n\n' }
    ];
    docContent.push( comments );

    // Activities, Audience Types, Deliverables
    var activityLists = [{
      columns: [
        {
          text: [
            { text: 'Activity Types\n', style: 'tableHeader' },
            { text: activityString }
          ]
        },
        {
          text: [
            { text: 'Audience Types\n', style: 'tableHeader' },
            { text: audienceString }
          ]
        },
        {
          text: [
            { text: 'Deliverables\n', style: 'tableHeader' },
            { text: deliverableString }
          ]
        }
      ]
    }];
    docContent.push( activityLists );

    this.dd = {
      content: docContent,
      styles: this.helperPDFService.getPDFStyles(),
      defaultStyle: this.helperPDFService.getPDFDefaultStyles()
    };

    this.generatePDFFile();
  }

  generatePDFFile()
  {
    if ( this.platform.is('cordova') )
    {
      pdfMake.createPdf( this.dd ).getBuffer((buffer) => {
        var utf8 = new Uint8Array(buffer); // Convert to UTF-8...
        let binaryArray = utf8.buffer; // Convert to Binary...
        let saveDir = this.file.dataDirectory;

        this.file.createFile(saveDir, this.pdfFileName, true).then((fileEntry) => {
          fileEntry.createWriter((fileWriter) => {
            fileWriter.onwriteend = () => {
              // Get rid of all the toast crap and just
              // display the PDF.
              this.fileOpener.open( saveDir + this.pdfFileName, 'application/pdf');
            };

            fileWriter.onerror = (e) => {
              console.log('file writer - error event fired: ' + e.toString());
            };

            fileWriter.write(binaryArray);

           });
         });
      });
    } else {
      pdfMake.createPdf( this.dd ).download( this.pdfFileName );
    }

    this.goBack();
  }

  goBack()
  {
    this.navCtrl.pop();
  }

  ionViewDidEnter()
  {
    this.createPDFDocumentDefinition();
  }
}
