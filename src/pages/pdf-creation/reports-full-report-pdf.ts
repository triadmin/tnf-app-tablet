import { Component, ViewChildren, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, NavParams, Platform, Events, ToastController } from 'ionic-angular';

// Ionic Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { Constants } from '../../providers/config/config';
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';

import { ReportService } from '../../providers/report-service/report-service';
import { HelperService } from '../../providers/helper-service/helper-service';
import { HelperPDFService } from '../../providers/helper-service/helper-pdf-service';
import { ImageService } from '../../providers/image-service/image-service';

// Ionic Native
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

// Chart types
import { DonutComponent } from '../../components/chart_types/donut';
import { BarComponent } from '../../components/chart_types/bar';
import { BarSimpleComponent } from '../../componenets/chart_types/bar-simple';
import { ActivityDurationReportComponent } from '../../components/chart_types/activity-duration';

// PDF Make
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

@Component({
  templateUrl: 'reports-full-report-pdf.html'
})

export class ReportsFullReportPDFPage {
  @ViewChild('chart1') chart1: DonutComponent;

  // Document definition var for pdfMake
  dd: any;
  reportTitle: any;
  reportSubTitle: any;
  selectedQuarter: any;
  selectedRegion: any;
  filteredActivities: any;
  reportType: any;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private events: Events,
    private platform: Platform,
    private reportService: ReportService,
    private helperService: HelperService,
    private helperPDFService: HelperPDFService,
    private toastCtrl: ToastController,
    private ga: GoogleAnalytics,
    private file: File,
    private fileOpener: FileOpener,
    private imageService: ImageService
  ) {
    this.reportType = this.navParams.get('reportType');
    this.filteredActivities = ReportDataStore.REPORT_FILTERED_ACTIVITIES;
    this.ga.trackView('Reports Create Report PDF');
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  renderReportOnScreen()
  {
    this.reportTitle = 'TNF Activity Report: ' + ReportDataStore.REPORT_PERIOD;
    this.selectedQuarter = ReportDataStore.REPORT_PERIOD;
    this.selectedRegion = 'All Regions';

    if ( ReportDataStore.REPORT_FILTER_OBJECT.selectedRegion !== '' )
    {
      this.selectedRegion = ReportDataStore.REPORT_FILTER_OBJECT.selectedRegion;
    }
    this.reportSubTitle = this.selectedRegion;

    if ( ReportDataStore.REPORT_FILTER_OBJECT.selectedCounty !== '' )
    {
      this.reportSubTitle += ' > ' + ReportDataStore.REPORT_FILTER_OBJECT.selectedCounty;
    }
    if ( ReportDataStore.REPORT_FILTER_OBJECT.selectedDistrict !== '' )
    {
      this.reportSubTitle += ' > ' + ReportDataStore.REPORT_FILTER_OBJECT.selectedDistrict;
    }

    // Other filters
    if ( ReportDataStore.REPORT_FILTER_OBJECT.selectedActivities.length > 0 )
    {
      var activityFilterString = '';
      var selectedActivities = ReportDataStore.REPORT_FILTER_OBJECT.selectedActivities;
      for ( var activity in selectedActivities )
      {
        activityFilterString += selectedActivities[activity] + ' ';
      }
      this.reportSubTitle += '\nActivities Selected: ' + activityFilterString;
    }

    // Render SVG charts so we can grab them later.
    //this.chart1.refreshChartData( ReportDataStore.REPORT_FILTER_OBJECT.selectedQuarter );

    // wait for a second or two for chart animations
    // to complete and then...
    setTimeout(() => {
      switch( this.reportType )
      {
        case 'activityList':
          this.createActivityListPDFDocDefinition();
          break;
        case 'frequencyReport':
          this.createFrequencyPDFDocDefinition();
          break;
      }
      //this.createReportPDFDocumentDefinition();
    }, 2000);
  }

  createActivityListPDFDocDefinition()
  {
    // Main Activity Entry tables
    var dataActivityEntryTable = [];

    // All Activity Entry table
    // Date | Title | Time Spent | TA Mode | Location(s)
    var headerRow = [
      { text: 'Date', style: 'tableHeader' },
      { text: 'Activity Title', style: 'tableHeader'},
      { text: 'Time', style: 'tableHeader' },
      { text: 'TA Mode', style: 'tableHeader' },
      { text: 'Locations', style: 'tableHeader'}
    ];
    dataActivityEntryTable.push( headerRow );

    for ( var activity in this.filteredActivities )
    {
      var dataRow = [];
      var name = this.filteredActivities[activity].User.UsersFirstName + ' ' + this.filteredActivities[activity].User.UsersLastName;
      var locationString = this.filteredActivities[activity].User.UsersRegion + ' (' + name + ')\n';
      if ( this.filteredActivities[activity].Location.Counties.length > 0 )
      {
        for ( var county in this.filteredActivities[activity].Location.Counties )
        {
          locationString += this.filteredActivities[activity].Location.Counties[county] + ' ';
        }
      }
      if ( this.filteredActivities[activity].Location.Districts.length > 0 )
      {
        for ( var district in this.filteredActivities[activity].Location.Districts )
        {
          locationString += this.filteredActivities[activity].Location.Districts[district] + ' ';
        }
      }

      var taMode = this.filteredActivities[activity].TechnicalAssistanceMode;
      if ( taMode !== 'On-Site' )
      {
        taMode = 'Electronic';
      }
      dataRow.push( { text: this.filteredActivities[activity].DateFormatted, style: 'normal' } );
      dataRow.push( { text: this.filteredActivities[activity].SurveyAttemptsTitle, style: 'normal' } );
      dataRow.push( { text: this.filteredActivities[activity].Duration, style: 'normal' } );
      dataRow.push( { text: taMode, style: 'normal' } );
      dataRow.push( { text: locationString, style: 'normal' } );

      dataActivityEntryTable.push( dataRow );
    }

    // Make PDF document definition object.
    var docContent = [];

    // Header
    var dataHeader = {
      reportTitle: this.reportTitle,
      reportSubTitle: this.reportSubTitle
    };
    docContent.push( this.helperPDFService.makePDFHeader( dataHeader ) );

    // Activity Entry table
    docContent.push( this.helperPDFService.makeActivityEntryTable( dataActivityEntryTable ) );

    this.dd = {
      content: docContent,
      styles: this.helperPDFService.getPDFStyles(),
      defaultStyle: this.helperPDFService.getPDFDefaultStyles()
    };

    this.generatePDFFile( 'TNFReport-Activity-List.pdf' );
  }

  createFrequencyPDFDocDefinition()
  {
    var frequencyData = this.reportService.formatFrequencyReports( this.filteredActivities );

    var chartObjActivity = this.reportService.formatReportData( frequencyData.dataTypeOfActivity, 'Activity', 1000 );
    var chartObjDeliverables = this.reportService.formatReportData( frequencyData.dataDeliverables, 'Deliverables', 1000 );
    var chartObjAudience = this.reportService.formatReportData( frequencyData.dataAudience, 'Audience', 1000 );
    var chartObjAreaInvolved = this.reportService.formatReportData( frequencyData.dataSite, 'Site', 1000 );

    // Frequency tables
    var dataActivityTable = [];
    var dataDeliverablesTable = [];
    var dataAudienceTable = [];
    var dataAreaCountyTable = [];
    var dataAreaDistrictTable = [];

    // Activity Frequency table
    var headerRow = [ { text: 'Activity', style: 'tableHeader' }, { text: 'Count', style: 'tableHeader'} ];
    dataActivityTable.push( headerRow );
    for ( var item in chartObjActivity.chartLabels )
    {
      var dataRow = [];
      dataRow.push( { text: chartObjActivity.chartLabels[item], style: 'normal' } );
      dataRow.push( { text: chartObjActivity.chartData[item].toString(), style: 'normal', alignment: 'right' } );

      dataActivityTable.push( dataRow );
    }

    // Deliverable Frequency table
    headerRow = [ { text: 'Deliverable', style: 'tableHeader' }, { text: 'Count', style: 'tableHeader'} ];
    dataDeliverablesTable.push( headerRow );
    for ( var item in chartObjDeliverables.chartLabels )
    {
      var dataRow = [];
      dataRow.push( { text: chartObjDeliverables.chartLabels[item], style: 'normal' } );
      dataRow.push( { text: chartObjDeliverables.chartData[item].toString(), style: 'normal', alignment: 'right' } );

      dataDeliverablesTable.push( dataRow );
    }

    // Audience Type Frequency table
    headerRow = [ { text: 'Audience Type', style: 'tableHeader' }, { text: 'Count', style: 'tableHeader'} ];
    dataAudienceTable.push( headerRow );
    for ( var item in chartObjAudience.chartLabels )
    {
      var dataRow = [];
      dataRow.push( { text: chartObjAudience.chartLabels[item], style: 'normal' } );
      dataRow.push( { text: chartObjAudience.chartData[item].toString(), style: 'normal', alignment: 'right' } );

      dataAudienceTable.push( dataRow );
    }

    // Area Involved (County) Frequency table
    headerRow = [ { text: 'Area Involved (County)', style: 'tableHeader' }, { text: 'Count', style: 'tableHeader'} ];
    dataAreaCountyTable.push( headerRow );
    for ( var item in chartObjAreaInvolved.chartLabels.county )
    {
      var dataRow = [];
      dataRow.push( { text: chartObjAreaInvolved.chartLabels.county[item], style: 'normal' } );
      dataRow.push( { text: chartObjAreaInvolved.chartData.county[item].toString(), style: 'normal', alignment: 'right' } );

      dataAreaCountyTable.push( dataRow );
    }

    // Area Involved (District) Frequency table
    headerRow = [ { text: 'Area Involved (District)', style: 'tableHeader' }, { text: 'Count', style: 'tableHeader'} ];
    dataAreaDistrictTable.push( headerRow );
    for ( var item in chartObjAreaInvolved.chartLabels.district )
    {
      var dataRow = [];
      dataRow.push( { text: chartObjAreaInvolved.chartLabels.district[item], style: 'normal' } );
      dataRow.push( { text: chartObjAreaInvolved.chartData.district[item].toString(), style: 'normal', alignment: 'right' } );

      dataAreaDistrictTable.push( dataRow );
    }

    // Make PDF document definition object.
    var docContent = [];

    // Header
    var dataHeader = {
      reportTitle: this.reportTitle,
      reportSubTitle: this.reportSubTitle
    };
    docContent.push( this.helperPDFService.makePDFHeader( dataHeader ) );

    // Activity, Audience, Deliverables
    var frequencyTables = {
      dataActivityTable: dataActivityTable,
      dataDeliverablesTable: dataDeliverablesTable,
      dataAudienceTable: dataAudienceTable
    };
    docContent.push( this.helperPDFService.makeFrequencyTables( frequencyTables ) );

    // AreaInvolved distribtion tables.
    var areaTables = {
      dataAreaCountyTable: dataAreaCountyTable,
      dataAreaDistrictTable: dataAreaDistrictTable
    };
    docContent.push( this.helperPDFService.makeAreaTables( areaTables ) );


    // Test chart    
    //var chartData = this.chart1.getBase64Image();
    //docContent.push( this.helperPDFService.makeChart( chartData ) );
    

    this.dd = {
      content: docContent,
      styles: this.helperPDFService.getPDFStyles(),
      defaultStyle: this.helperPDFService.getPDFDefaultStyles()
    };

    this.generatePDFFile( 'TNFReport-Frequency.pdf' );
  }

  generatePDFFile( fileName )
  {
    if ( this.platform.is('cordova') )
    {
      pdfMake.createPdf( this.dd ).getBuffer((buffer) => {
        var utf8 = new Uint8Array(buffer); // Convert to UTF-8...
        let binaryArray = utf8.buffer; // Convert to Binary...

        let saveDir = this.file.dataDirectory;

        this.file.createFile(saveDir, fileName, true).then((fileEntry) => {
          fileEntry.createWriter((fileWriter) => {
            fileWriter.onwriteend = () => {
              // Get rid of all the toast crap and just
              // display the PDF.
              this.fileOpener.open( saveDir+fileName, 'application/pdf');
            };

            fileWriter.onerror = (e) => {
              console.log('file writer - error event fired: ' + e.toString());
            };

            fileWriter.write(binaryArray);

           });
         });
      });
    } else {
      pdfMake.createPdf( this.dd ).download( fileName );
    }

    this.goBack();
  }

  goBack()
  {
    this.navCtrl.pop();
  }

  ionViewDidEnter()
  {
    this.renderReportOnScreen();
  }
}
