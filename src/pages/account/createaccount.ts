import { Component } from '@angular/core';
import { App, NavController, Alert } from 'ionic-angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Constants } from '../../providers/config/config';
import { AccountService } from '../../providers/account-service/account-service';
import { LoginService } from '../../providers/login-service/login-service';
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  templateUrl: 'createaccount.html'
})
export class CreateAccountPage {
  accountError: boolean;
  accountErrorMessage: string;
  accountForm: any;
  firstname: string = '';
  lastname: string = '';
  email: string = '';
  password: string = '';
  region: string = '';

  constructor(
    private nav: NavController,
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private loginService: LoginService
  ) {
    this.formBuilder = formBuilder;
    this.accountService = accountService;
    this.loginService = loginService;
    this.nav = nav;

    this.accountForm = formBuilder.group({
        'firstname': ['', Validators.required],
        'lastname': ['', Validators.required],
        'email': ['', Validators.required],
        'password': ['', Validators.required],
        'region': ['', Validators.required]
    });

    this.firstname = this.accountForm.controls['firstname'];
    this.lastname = this.accountForm.controls['lastname'];
    this.email = this.accountForm.controls['email'];
    this.password = this.accountForm.controls['password'];
    this.region = this.accountForm.controls['region'];
  }

  onSubmit( accountData )
  {
    this.accountService.createAccount( accountData).subscribe(data => {
      let result = data.json();

      if ( result.errors.length > 0 )
      {
        this.accountError = true;
        switch ( result.errors[0].field )
        {
          case 'email':
            this.accountErrorMessage = 'The email address you entered is already associated with an account.'
            break;
          default:
            this.accountErrorMessage = result.errors[0].error;
        }
      } else {
        let access_token = result.data.access_token;
        if ( access_token )
        {
          // Save access token in database.
          this.loginService.saveCredentials( access_token );

          // Save access token in Constants.
          Constants.ACCESS_TOKEN = access_token;

          // Get user record.
          this.accountService.getUserRecord().subscribe((data) => {
            Constants.USER = data.json().data;
          });

          // Set the root page.
          this.nav.setRoot(DashboardPage, {
            message: 'This is your dashboard page.  This is what you can do here.'
          });
        } else {
          this.accountError = true;
        }
      }
    });
  }
}
