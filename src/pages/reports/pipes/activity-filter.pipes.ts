import { Pipe } from '@angular/core';
import { Constants } from '../../../providers/config/config';

@Pipe({
  name: 'filterPipeRegion'
})
export class FilterPipeRegion {
  transform(input, region?) {
    let result = input.filter( activity => {
      if ( region.length )
      {
        return activity.User.UsersRegion === region;
      } else {
        return true;
      }
    });
    return result;
  }
}

@Pipe({
  name: 'filterPipeCounty'
})
export class FilterPipeCounty {
  transform(input, county?) {
    let result = input.filter( activity => {
      if ( county.length )
      {
        return activity.Location.Counties.indexOf( county ) !== -1;
      } else {
        return true;
      }
    });
    return result;
  }
}

@Pipe({
  name: 'filterPipeDistrict'
})
export class FilterPipeDistrict {
  transform(input, district?) {
    let result = input.filter( activity => {
      if ( district.length )
      {
        return activity.Location.Districts.indexOf( district ) !== -1;
      } else {
        return true;
      }
    });
    return result;
  }
}

@Pipe({
  name: 'filterPipeSearch'
})
export class FilterPipeSearch {
  transform(input, searchTerm?) {
    let result = input.filter( activity => {
      if ( searchTerm && searchTerm.length )
      {
        return (
          activity.SurveyAttemptsTitle.toLowerCase().indexOf( searchTerm.toLowerCase() ) > -1 ||
          activity.Comments.toLowerCase().indexOf( searchTerm.toLowerCase() ) > -1
        );
      } else {
        return true;
      }
    });

    return result;
  }
}

@Pipe({
  name: 'filterPipeActivityType'
})
export class FilterPipeActivityType {
  transform(input, activityType?) {
    let result = input.filter( activity => {
      if ( activityType.length )
      {
        for ( let at of activityType )
        {
          if ( at.length && activity.TypeOfActivity.indexOf( at ) !== -1 )
          {
            return activity;
          }
        }
      } else {
        return true;
      }
    });

    return result;
  }
}

@Pipe({
  name: 'filterPipeDeliverable'
})
export class FilterPipeDeliverable {
  transform(input, deliverable?) {
    let result = input.filter( activity => {
      if ( deliverable.length )
      {
        for ( let d of deliverable )
        {
          if ( d.length && activity.Deliverables.indexOf( d ) !== -1 )
          {
            return activity;
          }
        }
      } else {
        return true;
      }
    });

    return result;
  }
}

@Pipe({
  name: 'filterPipeAudience'
})
export class FilterPipeAudience {
  transform(input, audience?) {
    let result = input.filter( activity => {
      if ( audience.length )
      {
        for ( let a of audience )
        {
          if ( a.length && activity.Audience.indexOf( a ) !== -1 )
          {
            return activity;
          }
        }
      } else {
        return true;
      }
    });

    return result;
  }
}
