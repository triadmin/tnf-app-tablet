import { Component } from '@angular/core';
import { Events } from 'ionic-angular';
import { Constants } from '../../../providers/config/config';
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'report-widgets',
  templateUrl: 'report-widget.component.html'
})

export class ReportWidgetComponent
{
  user: any;
  filteredActivities: any = [];
  reportElementData: any = [];
  totalActivities: number;

  widgetChartData: any = {
    percentageOfTotal: {
      chartData: [0,0],
      labelData: [0,0]
    },
    percentageOfFollowUpVisits: {
      chartData: [0,0],
      labelData: [0,0]
    },
    percentageOfOnSiteVisits: {
      chartData: [0,0],
      labelData: [0,0]
    }
  };

  doughnutChartOptions: any = [];
  doughnutChartType: string = 'doughnut';
  doughnutChartColors: any = [
    { backgroundColor: ['#a1d489', '#d7d9db'] }
  ];
  doughnutChartColorsFiltered: any = [
    { backgroundColor: ['#f9831c', '#d7d9db'] }
  ];

  constructor(
    private events: Events
  )
  {
    this.user = Constants.USER;

    this.events.subscribe('reports:updatereportelements', ( reportElementData ) => {
      this.reportElementData = reportElementData;
      this.updateReportElements();
    });

    this.doughnutChartOptions = {
      responsive: false,
      events: ['click'],
      cutoutPercentage: 75,
      legend: {
        display: false
      }
    };
  }

  updateReportElements()
  {
    let totalActivityCount = this.reportElementData.totalActivityCount;
    let totalFilteredActivities = this.reportElementData.filteredActivities.length;

    // Update percentage of total chart
    let filteredPercentage = Math.round(
      ( totalFilteredActivities / totalActivityCount) * 100
    );

    this.widgetChartData.percentageOfTotal.chartData = [
      totalFilteredActivities,
      (totalActivityCount - totalFilteredActivities)
    ];

    this.widgetChartData.percentageOfTotal.labelData = [
      filteredPercentage,
      totalActivityCount
    ];

    // Update Follow-Ups chart
    let totalFollowUps = 0;
    this.reportElementData.filteredActivities.forEach(function ( activity ) {
      totalFollowUps = parseInt( totalFollowUps + activity.FollowUpSurveyAttempts.length );
    });
    let followUpPercentage = Math.round(
      ( totalFollowUps / totalFilteredActivities) * 100
    );
    if ( isNaN( followUpPercentage ) )
    {
      followUpPercentage = 0;
    }

    this.widgetChartData.percentageOfFollowUpVisits.chartData = [
      totalFollowUps,
      (totalFilteredActivities - totalFollowUps)
    ];

    this.widgetChartData.percentageOfFollowUpVisits.labelData = [
      followUpPercentage,
      totalFilteredActivities
    ];

    // Update On-Site chart
    let totalOnSite = 0;
    this.reportElementData.filteredActivities.forEach(function ( activity ) {
      if ( activity.TechnicalAssistanceMode === 'On-Site' )
      {
        totalOnSite++;
      }
    });
    let onSitePercentage = Math.round(
      ( totalOnSite / totalFilteredActivities) * 100
    );
    if ( isNaN( onSitePercentage ) )
    {
      onSitePercentage = 0;
    }

    this.widgetChartData.percentageOfOnSiteVisits.chartData = [
      totalOnSite,
      (totalFilteredActivities - totalOnSite)
    ];

    this.widgetChartData.percentageOfOnSiteVisits.labelData = [
      onSitePercentage,
      totalFilteredActivities
    ];
  }
}
