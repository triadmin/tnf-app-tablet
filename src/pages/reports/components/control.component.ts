import { Component, Input, Output, OnInit } from '@angular/core';
import { PopoverController, Events } from 'ionic-angular';
import { Constants } from '../../../providers/config/config';
import { ReportDataStore } from '../../../providers/report-service/report-datastore-service';
import { SurveyService } from '../../../providers/survey-service/survey-service';
import { HelperService } from '../../../providers/helper-service/helper-service';

@Component({
  selector: 'report-filter-control',
  templateUrl: 'control.component.html'
})

export class ReportFilterControlComponent
{
  regions: any = [];
  counties: any = [];
  districts: any = [];
  quarters: any = Constants.DATA_QUARTERS;
  showRegionSelect = false;

  filterObj: any = ReportDataStore.REPORT_FILTER_OBJECT;

  user: any;
  activityTypes: any = [];
  audiences: any = [];
  deliverables: any = [];

  constructor(
    private popoverCtrl: PopoverController,
    private events: Events,
    private surveyService: SurveyService,
    private helperService: HelperService
  )
  {
    this.user = Constants.USER;

    // Set our filterObj init state
    this.setInitControlState();
    this.populateQuestionTypeFilters();

    // Do we already have a filterObj in ReportDataStore?
    if ( !this.helperService.isObjectEmpty( ReportDataStore.REPORT_FILTER_OBJECT ) )
    {
      // Why yes we do!
      this.filterObj = ReportDataStore.REPORT_FILTER_OBJECT;
      this.filterActivities( null );
    } else {
      // Nope, we sure don't.
      this.filterActivities( 'quarter' );
    }

    // Listen for filter events from activity box.
    this.events.subscribe('reports:filteractivities-from-activity-box', ( filterObjActivityBox ) => {
      this.filterObj = filterObjActivityBox;
      this.updateControlState();
      this.filterActivities( filterObjActivityBox.filterType );
    });
  }

  setInitControlState()
  {
    // We can initially only set Region and Quarter.
    if ( this.user.UsersRole === 'Survey Report Admin' )
    {
      for ( var region in Constants.REGION_DATA[0] ) {
        this.regions.push( region );
      }
      this.showRegionSelect = true;
    } else {
      this.filterObj.selectedRegion = this.user.UsersRegion;
    }

    // Do we already have a selected quarter in ReportDataStore?
    if ( ReportDataStore.REPORT_PERIOD !== '' )
    {
      this.filterObj.selectedQuarter = ReportDataStore.REPORT_FILTER_OBJECT.selectedQuarter;
    } else {
      this.filterObj.selectedQuarter = this.quarters[0];
    }
  }

  populateQuestionTypeFilters()
  {
    this.surveyService.getSurveyQuestionPossibleAnswers( 'TypeOfActivity' ).subscribe(data => {
      let answers = data.json().data;
      answers.forEach((answer, index) => {
        this.activityTypes.push( answer.SurveyAnswersText );
      });
    });

    this.surveyService.getSurveyQuestionPossibleAnswers( 'Deliverables' ).subscribe(data => {
      let answers = data.json().data;
      answers.forEach((answer, index) => {
        this.deliverables.push( answer.SurveyAnswersText );
      });
    });

    this.surveyService.getSurveyQuestionPossibleAnswers( 'Audience' ).subscribe(data => {
      let answers = data.json().data;
      answers.forEach((answer, index) => {
        this.audiences.push( answer.SurveyAnswersText );
      });
    });
  }

  updateControlState()
  {
    // Update all filter controls based on filterObj.

    // If we have a selected Region, populate County select.
    if ( this.filterObj.selectedRegion.length > 0 )
    {
      this.counties.length = 0;
      let counties = Constants.REGION_DATA[0][this.filterObj.selectedRegion].counties;
      for ( var county in counties ) {
        this.counties.push( counties[county].county );
      }
    }

    // If we have a selected County, then populate District select.
    if ( this.filterObj.selectedCounty.length > 0 )
    {
      this.districts.length = 0;
      let counties = Constants.REGION_DATA[0][this.filterObj.selectedRegion].counties;

      for ( var county in counties )
      {
        if ( counties[county].county === this.filterObj.selectedCounty )
        {
          let districts = counties[county].districts;
          for ( var district in districts )
          {
            this.districts.push( districts[district].districtName );
          }
        }
      }
    }
  }

  filterActivities(filterType)
  {
    switch( filterType )
    {
      case 'region':
        this.filterObj.selectedCounty = '';
        this.filterObj.selectedDistrict = '';
        this.counties.length = 0;
        this.districts.length = 0;
        this.filterObj.dataFetch = false;
        break;
      case 'county':
        this.filterObj.selectedDistrict = '';
        this.districts.length = 0;
        this.filterObj.dataFetch = false;
        break;
      case 'quarter':
        this.filterObj.dataFetch = true;
        break;
      default:
        break;
    }
    // Set REPORT_FILTER_OBJECT in ReportDataStore
    ReportDataStore.REPORT_FILTER_OBJECT = this.filterObj;

    this.updateControlState();
    this.events.publish( 'reports:filteractivities', this.filterObj );

    if ( this.filterObj.selectedRegion !== '' )
    {
      this.events.publish( 'reports:updatemap' );
    }

    this.events.publish( 'reports:updatefrequencycharts' );
  }

  searchActivities( ev )
  {
    let search_term = ev.target.value;

    if ( search_term && search_term.length > 3 )
    {
      this.filterObj.searchTerm = search_term;
      this.events.publish('reports:filteractivities', this.filterObj);
    } else {
      this.clearSearchActivities( ev );
    }
  }

  clearSearchActivities( ev )
  {
    this.filterObj.searchTerm = '';
    this.events.publish('reports:filteractivities', this.filterObj);
  }
}
