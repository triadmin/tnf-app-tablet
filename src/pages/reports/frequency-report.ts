import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, ViewController, NavParams, Events } from 'ionic-angular';

// Ionic Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';

// Services
import { Constants } from '../../providers/config/config';
import { ReportService } from '../../providers/report-service/report-service';
import { UiService } from '../../providers/ui-service/ui-service';
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';

// Chart types
import { BarSimpleComponent } from '../../components/chart_types/bar-simple';

// Pages
import { MapReportPage } from './map-report';
import { ReportsFullReportPDFPage } from '../../pages/pdf-creation/reports-full-report-pdf';

@Component({
  templateUrl: 'frequency-report.html'
})

export class FrequencyReportPage
{
  @ViewChild('freqChart1') freqChart1: BarSimpleComponent;
  @ViewChild('freqChart2') freqChart2: BarSimpleComponent;
  @ViewChild('freqChart3') freqChart3: BarSimpleComponent;
  @ViewChild('freqChart4') freqChart4: BarSimpleComponent;
  @ViewChild('freqChart5') freqChart5: BarSimpleComponent;
  @ViewChild('reportDom') reportDom: ElementRef;

  frequencyType: string = 'activityType';
  selectedQuarter: any = {};
  selectedRegion: any;
  activities: any;
  filterObj: any = ReportDataStore.REPORT_FILTER_OBJECT;
  dataTypeOfActivity: any = [];
  dataAudience: any = [];
  dataDeliverables: any = [];
  dataSites: any = [];

  constructor(
    private navCtrl: NavController,
    private viewCtrl: ViewController,
    private params: NavParams,
    private reportService: ReportService,
    private uiService: UiService,
    private events: Events,
    private ga: GoogleAnalytics
  )
  {
    this.selectedRegion = ReportDataStore.REPORT_FILTER_OBJECT.selectedRegion;

    this.createChartData();
    this.ga.trackView('Frequency Reports');

    this.events.subscribe('reports:updatefrequencycharts', ( mapObj ) => {
      this.selectedRegion = ReportDataStore.REPORT_FILTER_OBJECT.selectedRegion;

      this.createChartData();
    });
  }

  createChartData()
  {
    this.activities = this.reportService.filterReportData( ReportDataStore.REPORT_ACTIVITIES, ReportDataStore.REPORT_FILTER_OBJECT );

    var frequencyData = this.reportService.formatFrequencyReports( this.activities );

    this.dataAudience = frequencyData.dataAudience;
    this.dataDeliverables = frequencyData.dataDeliverables;
    this.dataTypeOfActivity = frequencyData.dataTypeOfActivity;

    // Get site data
    this.dataSites = frequencyData.dataSite;

    this.populateCharts();
  }

  populateCharts()
  {
    setTimeout(() => {
      this.selectedQuarter = this.params.get( 'selectedQuarter' );
      this.freqChart1.refreshChartData( this.selectedQuarter, this.dataTypeOfActivity );
      this.freqChart2.refreshChartData( this.selectedQuarter, this.dataDeliverables );
      this.freqChart3.refreshChartData( this.selectedQuarter, this.dataAudience );

      this.freqChart4.refreshChartData( this.selectedQuarter, this.dataSites );
      this.freqChart5.refreshChartData( this.selectedQuarter, this.dataSites );
      this.showChart( this.frequencyType );
    }, 500);
  }

  showChart( chartType )
  {
    this.frequencyType = chartType;
  }

  presentReportPDFActionSheet()
  {
    let actionSheet = this.uiService.printReportActionSheet();
    actionSheet.present();
  }

  toggleMenu()
  {
    this.events.publish('menu:toggle');
  }

  presentMapPage()
  {
    this.navCtrl.push( MapReportPage );
  }
}
