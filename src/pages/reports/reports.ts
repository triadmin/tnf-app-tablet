import { Component, ViewChildren, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, NavParams, Events, ActionSheetController, ModalController, ToastController, LoadingController } from 'ionic-angular';

// Ionic Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';

// Services
import { Constants } from '../../providers/config/config';
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';
import { ReportService } from '../../providers/report-service/report-service';
import { HelperService } from '../../providers/helper-service/helper-service';
import { UiService } from '../../providers/ui-service/ui-service';

// Components
import { ActivityBoxComponent } from '../../components/activity-box.component';

// Pages
import { MapReportPage } from './map-report';
import { FrequencyReportPage } from './frequency-report';

@Component({
  templateUrl: 'reports.html'
})

export class ReportsPage {
  @ViewChildren('activities') filteredActivities: any;
  @ViewChild('reportDom') reportDom: ElementRef;

  filteredActivitiesArray: any = [];
  activityData: any = [];
  totalActivities: number = 0;
  selectedActivities: any = [];
  filterObj: any = ReportDataStore.REPORT_FILTER_OBJECT;
  loading: any;
  loadingToast: any;
  entryCoordinates: any;
  mapObj: any = {};
  dataLoaded: boolean = false;

  constructor(
    private navCtrl: NavController,
    private events: Events,
    private reportService: ReportService,
    private helperService: HelperService,
    private uiService: UiService,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private ga: GoogleAnalytics
  ) {
    this.events.unsubscribe('reports:filteractivities');

    // The report:filteractivities event is fired in the
    // control.componenets component in the filterActivities method.
    this.events.subscribe('reports:filteractivities', ( filterObjFromControl ) => {
      this.filterObj = filterObjFromControl;

      // Only load data if we have changed quarters OR
      // there is nothing in ReportDataStore.REPORT_ACTIVITIES
      if ( this.filterObj.dataFetch === true || ReportDataStore.REPORT_ACTIVITIES.length === 0 )
      {
        this.activityData.length = 0;
        ReportDataStore.REPORT_PERIOD = filterObjFromControl.selectedQuarter.period;
        this.getData( filterObjFromControl.selectedQuarter );
        ReportDataStore.REPORT_FILTER_OBJECT.dataFetch = false;
      } else {
        // When we filter, get a new array of our filtered
        // activities that we can user for reporting.
        setTimeout(() => {
          this.activityData = ReportDataStore.REPORT_ACTIVITIES;
          this.createFilteredActivitiesArray();
        }, 500);
      }
    });

    this.ga.trackView('Reports');
  }

  getData( quarter )
  {
    this.dataLoaded = false;
    this.presentLoadingToast();
    this.reportService.getAttemptData( quarter ).subscribe(data => {
      this.dataLoaded = true;
      this.formatData( data.json().data );
      this.dismissLoadingToast();
    });
  }

  formatData( data )
  {
    var newArray = [];
    let helperService = this.helperService;
    data.forEach(function ( activity ) {
      // Check for missing title
      if ( activity.SurveyAttemptsTitle === null || activity.SurveyAttemptsTitle.length === 0 )
      {
        activity.SurveyAttemptsTitle = '[No Title]';
      }

      // Meta data information
      activity = helperService.addMetaDataFlags(activity);

      activity.Location = {
        'Counties': [],
        'Districts': [],
        'LocationObject': [],
        'LocationJSON': ''
      };
      activity.Comments = '';
      activity.Duration = '';
      activity.TechnicalAssistanceMode = '';
      activity.TypeOfActivity = [];
      activity.Audience = [];
      activity.Deliverables = [];

      for( var response in activity.Responses )
      {
        var resp = activity.Responses[response];
        var qIdentifier = resp.SurveyQuestionsIdentifier;

        if ( qIdentifier == 'TechnicalAssistanceMode')
        {
          activity.TechnicalAssistanceMode = resp.SurveyResponsesText;
        }
        if ( qIdentifier == 'Comments')
        {
          activity.Comments = resp.SurveyResponsesText;
        }
        if ( qIdentifier == 'AreaInvolved' )
        {
          // Parse SurveyResponseText JSON
           try {
             activity.Location.LocationJSON = resp.SurveyResponsesText;
             var respObj = JSON.parse( resp.SurveyResponsesText );
             if ( respObj.counties && respObj.counties.length > 0 )
             {
               activity.Location.Counties = respObj.counties;
             }
             if ( respObj.districts && respObj.districts.length > 0 )
             {
               activity.Location.Districts = respObj.districts;
             }
             activity.Location.LocationObject = respObj;
           } catch (e) {
           }
        }
        if ( qIdentifier == 'DurationOfActivity' )
        {
          activity.Duration = resp.SurveyResponsesText;
        }
        if ( qIdentifier == 'Deliverables' )
        {
          activity.Deliverables.push( resp.SurveyResponsesText );
        }
        if ( qIdentifier == 'TypeOfActivity' )
        {
          activity.TypeOfActivity.push( resp.SurveyResponsesText );
        }
        if ( qIdentifier == 'Audience' )
        {
          activity.Audience.push( resp.SurveyResponsesText );
        }
      }
      newArray.push( activity );
    });

    this.activityData = newArray;

    // Make filteredActivitiesArray
    this.filteredActivitiesArray.length = 0;
    for ( let item in this.activityData )
    {
      this.filteredActivitiesArray.push( this.activityData[item] );
      // Push to ReportDataStore
      ReportDataStore.REPORT_ACTIVITIES.push( this.activityData[item] );
    }
    this.createFilteredActivitiesArray();
    this.updateReportElements();
  }

  // Create an array of just our filtered activities
  // that we can use for the report elements.
  createFilteredActivitiesArray()
  {
    this.filteredActivitiesArray = this.reportService.filterReportData( this.activityData, this.filterObj );
    ReportDataStore.REPORT_FILTERED_ACTIVITIES = this.filteredActivitiesArray;
    this.updateReportElements();
  }

  updateReportElements()
  {
    let reportElementData = {
      filteredActivities: this.filteredActivitiesArray,
      totalActivityCount: this.activityData.length
    };
    this.events.publish('reports:updatereportelements', reportElementData);
  }

  presentLoadingToast()
  {
    this.loadingToast = this.toastCtrl.create({
      message: 'Getting activities ... just a moment, please.',
      position: 'bottom',
      cssClass: 'toast-loading',
      showCloseButton: true,
      closeButtonText: 'Ok'
    });

    this.loadingToast.present();
  }

  dismissLoadingToast()
  {
    this.loadingToast.dismiss();
  }

  presentMapPage()
  {
    this.navCtrl.push( MapReportPage );
  }

  presentFrequencyPage()
  {
    this.navCtrl.push( FrequencyReportPage, {selectedQuarter: this.filterObj.selectedQuarter} );
  }

  presentReportPDFActionSheet()
  {
    let actionSheet = this.uiService.printReportActionSheet();
    actionSheet.present();
  }

  toggleMenu()
  {
    this.events.publish('menu:toggle');
  }

  ionViewWillEnter()
  {
    this.events.publish('reports:view');
  }

  ionViewWillLeave() {
    this.events.publish('reports:leave');
  }
}
