import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'heading',
  template: `
    <ion-list>
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
    </ion-list>
  `
})

export class HeadingComponent
{
  @Input() question: any;

  constructor( private surveyService: SurveyService ) {

  }
}
