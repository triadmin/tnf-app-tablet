import { Component, Input } from '@angular/core';
import { SurveyService } from '../../../providers/survey-service/survey-service';

@Component({
  selector: 'textareafield',
  template: `
  <ion-list>
    <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
    <ion-item>
      <ion-textarea placeholder="Enter comments here" autocorrect="on" rows="50" (blur)="saveResponse($event)" [(ngModel)]="response"></ion-textarea>
    </ion-item>
  </ion-list>
  `
})

export class TextareafieldComponent
{
  @Input() question: any;
  response: any;

  constructor( private surveyService: SurveyService ) {
    this.surveyService = surveyService;
  }

  saveResponse( event )
  {
    this.question.Responses = [];
    var answer = {
      'SurveyAnswersValue': '',
      'SurveyAnswersText': event.target.value,
      'SurveyAnswersSurveyQuestionsId': this.question.SurveyQuestionsId,
      'SurveyAnswersId': 0
    };
    this.surveyService.saveResponse( answer, 'text' ).subscribe(data => {
      this.question.Responses.push(data.json().data);
    });
  }

  renderResponse()
  {
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = responseObj[0].SurveyResponsesText;
    }
  }

  ngOnInit()
  {
    this.renderResponse();
  }
}
