import { Injectable } from '@angular/core';
import { Constants } from '../../providers/config/config';
import { UserService } from '../../providers/user-service/user-service';

@Injectable()
export class HelperService {
  user: any = Constants.USER;

  constructor( private userService: UserService ) {

  }

  createRandomString( length )
  {
    let randomString = '';
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < length; i++ )
        randomString += possible.charAt(Math.floor(Math.random() * possible.length));

    return randomString;
  }

  addMetaDataFlags( activity )
  {
    activity.ToDoStatus = 'complete';
    for ( var meta_data in activity.MetaData )
    {
      if ( activity.MetaData[meta_data].type === 'todos' )
      {
        activity.HasToDo = 'Yes';
        if ( activity.MetaData[meta_data].todos_status === 'incomplete' )
        {
          activity.ToDoStatus = 'incomplete';
        }
      }
      if ( activity.MetaData[meta_data].type === 'notification' )
      {
        activity.HasNotification = 'Yes';
      }
      if ( activity.MetaData[meta_data].type === 'tagforreport' )
      {
        activity.IsTaggedForReport = 'Yes';
      }
    }
    return activity;
  }

  addNoTitle( activity )
  {
    if ( activity.SurveyAttemptsTitle === '' )
    {
      activity.SurveyAttemptsTitle = '[No Title]';
    }
    return activity;
  }

  /*
    Get meta data for a given activity only.
  */
  getMetaDataItem( itemType, activity )
  {
    let metaDataObj = [];
    for ( let md in activity.MetaData )
    {
      let metaDataItem = activity.MetaData[md];
      switch ( itemType )
      {
        case 'todos':
          if ( metaDataItem.type === 'todos' )
          {
            metaDataObj.push( metaDataItem );
          }
          break;
        case 'notification':
          if ( metaDataItem.type === 'notification' )
          {
            var recipientId = metaDataItem.notification_recipient;
            if ( recipientId !== this.user.UsersId )
            {
              this.userService.getUser( recipientId ).subscribe(data => {
                let user = data.json().data;
                metaDataItem.notification_to_from = 'To ' + user.UsersFirstName + ' ' + user.UsersLastName + ' on ' + metaDataItem.date;
              });
            }
            var authorId = metaDataItem.notification_from;
            if ( authorId !== this.user.UsersId )
            {
              this.userService.getUser( authorId ).subscribe(data => {
                let user = data.json().data;
                metaDataItem.notification_to_from = 'From ' + user.UsersFirstName + ' ' + user.UsersLastName + ' on ' + metaDataItem.date;
              });
            }
            metaDataObj.push( metaDataItem );
            break;
          }
      }
    }
    // Order by date, DESC
    metaDataObj.sort( function( a,b )
    {
      return new Date(b.dateSortable).getTime() - new Date(a.dateSortable).getTime()
    });

    return metaDataObj;
  }

  isObjectEmpty( obj )
  {
    // Returns true if obj is empty {}
    for( var key in obj ) {
      if( obj.hasOwnProperty(key) )
        return false;
    }
    return true;
  }

  /*
    When passed a district, this method will return
    the districts' county object.
   */
  getCountyFromDistrict( district )
  {
    for ( var region in Constants.REGION_DATA[0] )
    {
      let counties = Constants.REGION_DATA[0][region].counties;

      for ( var county in counties )
      {
        let districts = counties[county].districts;

        for ( var dist in districts )
        {
          if ( districts[dist].districtName === district )
          {
            return counties[county];
          }
        }
      }
    }

    return false;
  }

  /*
    When passed a district or a county as a location string, this method will return
    the districts' or counties region object.
   */
  getRegionFromDistrictCounty( location )
  {
    var countyObj = this.getCountyFromDistrict( location );
    var countyInput = '', districtInput = '';

    // If countyObj = false, then we've passed in a county.
    if ( !countyObj )
    {
      // Then location is a county.
      countyInput = location;
    } else {
      // Then location is a district.
      districtInput = location;
      countyInput = countyObj.county;
    }

    // Now find Region from county.
    for ( var region in Constants.REGION_DATA[0] )
    {
      let counties = Constants.REGION_DATA[0][region].counties;
      for ( var county in counties )
      {
        if ( counties[county].county === countyInput )
        {
          let locationObj = {
            region: region,
            county: countyInput,
            district: districtInput
          };
          return locationObj;
        }
      }
    }
    return false;
  }
}
