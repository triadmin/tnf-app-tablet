export let ReportDataStore = {
  REPORT_FILTERED_ACTIVITIES: [],
  REPORT_ACTIVITIES: [],
  REPORT_FILTER_OBJECT: {
    selectedRegion: '',
    selectedCounty: '',
    selectedDistrict: '',
    selectedQuarter: {},
    selectedActivities: [],
    selectedAudiences: [],
    selectedDeliverables: [],
    searchTerm: '',
    dataFetch: false
  },
  REPORT_PERIOD: ''
};
