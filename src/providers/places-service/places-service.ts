import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class PlacesService {
  http: Http;
  api_key: string = Constants.GOOGLE_PLACES_API_KEY;
  survey_id: number = Constants.SURVEY_ID;
  server_url: string = Constants.SERVER_URL;

  constructor( http: Http ) {
    this.http = http;
  }

  getUserPlaces()
  {
    var url = Constants.SERVER_URL + 'surveyreports/report_data/' + this.survey_id + '/AreaInvolved?access_token=' + Constants.ACCESS_TOKEN;
    return this.http.get(url);
  }

  // Get list of nearby places based on user location (lat, long)
  getPlaces( lat, long )
  {
    var url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' + lat + ',' + long + '&radius=5000&key=' + this.api_key;
    return this.http.get(url);
  }

  // Get lat, long from place name
  getCoords( placeName )
  {
    if ( placeName.length > 0 )
    {
      var url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?query=' + placeName[0] + '&key=' + this.api_key;
      return this.http.get(url);
    } else {
      return;
    }
  }
}
