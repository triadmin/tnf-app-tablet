import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class NotificationsService {
  http: Http;
  constructor( http: Http ) {
    this.http = http;
  }

  /*
    Get all Notifications for all activies, organized by
    activity. This is for use on the dashboard.
    status can be read, not read, all.
   */
  getAllNotifications( status )
  {
    return this.http.get(Constants.SERVER_URL + 'tnfapp/all_notifications_by_status?access_token=' + Constants.ACCESS_TOKEN + '&read=' + status);
  }

  markNotificationRead( entryId )
  {
    return this.http.get(Constants.SERVER_URL + 'surveymetadata/update_meta_data_key_value?access_token=' + Constants.ACCESS_TOKEN + '&SurveyMetaDataEntryId=' + entryId + '&SurveyMetaDataKey=notification_read&SurveyMetaDataValue=Yes');
  }
}
