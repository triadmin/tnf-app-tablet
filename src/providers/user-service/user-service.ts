import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class UserService {
  http: Http;
  constructor( http: Http ) {
    this.http = http;
  }

  getUser( userId )
  {
    return this.http.get(Constants.SERVER_URL + 'users/user/' + userId + '?access_token=' + Constants.ACCESS_TOKEN);
  }
}
