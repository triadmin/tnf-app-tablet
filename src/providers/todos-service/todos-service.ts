import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class ToDosService {
  http: Http;
  constructor( http: Http ) {
    this.http = http;
  }

  /*
    Get all ToDo's for all activies, organized by
    activity. This is for use on the dashboard.
    status can be incomplete, complete, all.
   */
  getAllToDos( status )
  {
    return this.http.get(Constants.SERVER_URL + 'tnfapp/all_todos_by_status?access_token=' + Constants.ACCESS_TOKEN + '&status=' + status);
  }

  deleteToDo( entryId )
  {
    return this.http.get(Constants.SERVER_URL + 'surveymetadata/delete_meta_data?access_token=' + Constants.ACCESS_TOKEN + '&SurveyMetaDataEntryId=' + entryId);
  }

  toggleToDoComplete( entryId, currentStatus)
  {
    let newStatus = 'complete';
    if ( currentStatus === 'complete' )
    {
      newStatus = 'incomplete';
    }
    return this.http.get(Constants.SERVER_URL + 'surveymetadata/update_meta_data_key_value?access_token=' + Constants.ACCESS_TOKEN + '&SurveyMetaDataEntryId=' + entryId + '&SurveyMetaDataKey=todos_status&SurveyMetaDataValue=' + newStatus);
  }
}
