import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class SurveyService {
  http: Http;
  survey_id: number = Constants.SURVEY_ID;
  server_url: string = Constants.SERVER_URL;

  constructor( http: Http ) {
    this.http = http;
  }

  getSurvey()
  {
    var url = Constants.SERVER_URL + 'survey/survey/' + this.survey_id + '?access_token=' + Constants.ACCESS_TOKEN;

    if ( Constants.SURVEY_ATTEMPT_ID > 0 )
    {
      url = Constants.SERVER_URL + 'survey/survey/' + this.survey_id + '/' + Constants.SURVEY_ATTEMPT_ID + '?access_token=' + Constants.ACCESS_TOKEN
    }
    return this.http.get(url);
  }

  saveResponse( response, qtype )
  {
    // Set REFRESH_DASHBOARD = true so our dashboard will get
    // refreshed next time we see it.
    Constants.REFRESH_DASHBOARD = true;

    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});

    var save_response = 'Yes';
    if ( response.ischecked == false )
    {
      save_response = 'No';
    }
    let data = 'SurveyResponsesSurveyAttemptsId=' + Constants.SURVEY_ATTEMPT_ID + '&SurveyResponsesQuestionsId=' + response.SurveyAnswersSurveyQuestionsId + '&SurveyResponsesText=' + response.SurveyAnswersText +
        '&SurveyResponsesValue=' + response.SurveyAnswersValue +
        '&SurveyResponsesAnswersId=' + response.SurveyAnswersId +
        '&QType=' + qtype + '&SaveResponse=' + save_response;

    return this.http.post(Constants.SERVER_URL + 'survey/save_response?access_token=' + Constants.ACCESS_TOKEN, data, options);
  }

  saveSurveyAttemptResponse( answerObj )
  {
    // Set REFRESH_DASHBOARD = true so our dashboard will get
    // refreshed next time we see it.
    // TODO: Replace this with an event publish.
    Constants.REFRESH_DASHBOARD = true;

    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});

    let data = 'SurveyAttemptsId=' + answerObj.survey_attempt_id + '&QuestionType=' + answerObj.question_type;
    if ( answerObj.question_type == 'surveyTitle' )
    {
      data += '&SurveyAttemptsTitle=' + answerObj.answer;
    } else if ( answerObj.question_type == 'requiresFollowUp' ) {
      data += '&SurveyAttemptsFollowUpRequired=' + answerObj.answer;
    } else if ( answerObj.question_type == 'surveyDate' ) {
      data += '&SurveyAttemptsSurveyDate=' + answerObj.answer;
    } else {
      data += '&SurveyAttemptsFollowUpToId=' + answerObj.answer;
    }

    return this.http.post(Constants.SERVER_URL + 'survey/update_survey_attempt?access_token=' + Constants.ACCESS_TOKEN, data, options);
  }

  createSurveyAttempt()
  {
    return this.http.get(Constants.SERVER_URL + 'survey/create_survey_attempt/' + this.survey_id + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  deleteSurveyAttempt( surveyAttemptId )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/delete_survey_attempt/' + surveyAttemptId + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  getSurveyAttempts( withFollowUps, offset, limit, marked_for_follow_up = '' )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempts_for_user/' + Constants.SURVEY_ID + '?access_token=' + Constants.ACCESS_TOKEN + '&offset=' + offset + '&limit=' + limit + '&with_follow_ups=' + withFollowUps + '&marked_for_follow_up=' + marked_for_follow_up);
  }

  countSurveyAttempts()
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempts_for_user_count/' + Constants.SURVEY_ID + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  searchSurveyAttempts( search_term, withFollowUps )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_search/' + Constants.SURVEY_ID + '?access_token=' + Constants.ACCESS_TOKEN + '&search_term=' + search_term + '&with_follow_ups=' + withFollowUps);
  }

  getSurveyAttempt( surveyAttemptId )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempt/' + surveyAttemptId + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  getSurveyAttemptDateRange()
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempt_date_range/' + Constants.SURVEY_ID + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  getSurveyAttemptQuarters()
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempt_quarters/' + Constants.SURVEY_ID + '/1?access_token=' + Constants.ACCESS_TOKEN);
  }

  saveSurveyMetaData( data )
  {
    // data object contains SurveyId, SurveyAttemptId, Key, Value
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});

    let params = new URLSearchParams();
    for ( let key in data )
    {
      params.set(key, data[key]);
    }
    return this.http.post(Constants.SERVER_URL + 'survey/save_meta_data?access_token=' + Constants.ACCESS_TOKEN, params.toString(), options);
  }

  deleteSurveyMetaData( surveyAttemptId, surveyMetaDataKey )
  {
    return this.http.get(Constants.SERVER_URL + 'surveymetadata/delete_meta_data?SurveyMetaDataKey=' + surveyMetaDataKey + '&SurveyMetaDataSurveyAttemptId=' + surveyAttemptId + '&access_token=' + Constants.ACCESS_TOKEN);
  }

  getSurveyMetaData( surveyAttemptId )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempt_meta_data/' + surveyAttemptId + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  sendSurveyNotification( surveyAttemptId, recipientId, senderId )
  {
    return this.http.get(Constants.SERVER_URL + 'tnfapp/send_notification' + '?access_token=' + Constants.ACCESS_TOKEN + '&surveyAttemptId=' + surveyAttemptId + '&recipientId=' + recipientId + '&senderId=' + senderId);
  }

  getSurveyFollowUpAttempts( surveyAttemptId )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_attempt_follow_up_attempts/' + surveyAttemptId + '?access_token=' + Constants.ACCESS_TOKEN);
  }

  getSurveyQuestionPossibleAnswers( questionIdentifier )
  {
    return this.http.get(Constants.SERVER_URL + 'survey/survey_question_answers?access_token=' + Constants.ACCESS_TOKEN + '&questionIdentifier=' + questionIdentifier + '&surveyId=' + this.survey_id);
  }
}
