import { Injectable } from '@angular/core';

import { App, ActionSheetController, NavParams, ModalController } from 'ionic-angular';

import { ReportsFullReportPDFPage } from '../../pages/pdf-creation/reports-full-report-pdf';

@Injectable()
export class UiService {

  constructor(
    public app: App,
    public actionSheetCtrl: ActionSheetController,
    public modalCtrl: ModalController
  ) {}

  printReportActionSheet()
  {
    var nav = this.app.getActiveNav();
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select a Report to Print',
      buttons: [
        {
          text: 'Activity List Report',
          handler: () => {
            let printModal = this.modalCtrl.create( ReportsFullReportPDFPage, {
              reportType: 'activityList'
            });
            printModal.present();
          }
        },
        {
          text: 'Activity Frequency Report',
          handler: () => {
            let printModal = this.modalCtrl.create( ReportsFullReportPDFPage, {
              reportType: 'frequencyReport'
            });
            printModal.present();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });
    return actionSheet;
  }
}
