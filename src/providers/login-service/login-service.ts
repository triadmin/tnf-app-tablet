import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Constants } from '../../providers/config/config';
import 'rxjs/Rx';

@Injectable()
export class LoginService {

  constructor(
    private http: Http,
    private platform: Platform,
    private storage: Storage
  ) {

  }

  getCredentials()
  {
    return this.storage.get('access_token');
  }

  saveCredentials( access_token )
  {
    return this.storage.set('access_token', access_token);
  }

  tryLogin( email, password )
  {
    // Try login at the server using the API.
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});
		let data = 'email=' + email + '&password=' + password;

    return this.http.post(Constants.SERVER_URL + 'auth?access_token=1', data, options);
  }

  resetPassword( email )
  {
    // Try login at the server using the API.
    let headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		});
		let options = new RequestOptions({
			headers: headers
		});
		let data = 'email=' + email;

    return this.http.post(Constants.SERVER_URL + 'auth/reset_password?access_token=1', data, options);
  }

  logout()
  {
    return this.storage.set('access_token', null);
  }
}
