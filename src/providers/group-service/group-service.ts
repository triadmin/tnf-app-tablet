import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Constants } from '../../providers/config/config';
import { Storage } from '@ionic/storage';
import 'rxjs/Rx';

@Injectable()
export class GroupService {
  group_id: number = Constants.GROUP_ID;

  constructor( 
    private http: Http,
    private storage: Storage
  ) 
  {

  }

  // This method is used to provide global 
  // notifications to app users.
  getLastGroupForumPost()
  {
    var url = Constants.SERVER_URL + 'forum/forum_post_latest/' + this.group_id;
    return this.http.get(url);
  }

  getLastPostViewed( postId )
  {
    return this.storage.get('last_forum_post_viewed');
  }

  markPostAsViewed( postId )
  {
    this.storage.set('last_forum_post_viewed', postId);
  }
}  