import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, ModalController, Platform, ToastController, AlertController } from 'ionic-angular';

// Ionic Native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Network } from '@ionic-native/network';

// Pages
import { LoginPage } from '../pages/login/login';
import { ActivityViewPage } from '../pages/activity/activity-view';
import { DashboardPage } from '../pages/dashboard/dashboard';

// Constants
import { Constants } from '../providers/config/config';

// Services
import { AppDataService } from '../providers/app-data-service/app-data-service';
import { AccountService } from '../providers/account-service/account-service';
import { LoginService } from '../providers/login-service/login-service';
import { HelperService } from '../providers/helper-service/helper-service';

// Components
import { ActivityListComponent } from '../pages/activity/components/activity-list.component';
import { ReportFilterControlComponent } from '../pages/reports/components/control.component';
import { ReportWidgetComponent } from '../pages/reports/components/report-widget.component';

// Ionic native CodePush
import { CodePush } from '@ionic-native/code-push';

@Component({
  templateUrl: 'app.html'
})
export class TnfApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  activeMenu: string = 'blankMenu';

  constructor(
    public menu: MenuController,
    public platform: Platform,
    public events: Events,
    public toastCtrl: ToastController,
    public addDataService: AppDataService,
    public accountService: AccountService,
    public loginService: LoginService,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public ga: GoogleAnalytics,
    public network: Network,
    public alertCtrl: AlertController,
    public helperService: HelperService,
    public codePush: CodePush,
    public modalCtrl: ModalController
  ) {
    this.initializeApp();
    this.listenToEvents();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if ( this.platform.is('cordova') )
      {

        this.statusBar.styleDefault();
        this.statusBar.overlaysWebView(true);
        this.statusBar.backgroundColorByHexString('#ffffff');
        this.statusBar.styleLightContent();
        this.ga.startTrackerWithId(Constants.GOOGLE_ANALYTICS_TRACKING_ID )
          .then(() => {
            this.ga.trackView('App Opening Screen');
            this.ga.setAppVersion(Constants.APP_VERSION);
          });

        this.watchNetwork();

        // This must be at the bottom ALL THE TIME or else the app
        // will hang after splash screen.
        this.hideSplashScreen();

        // Set up CodePush
        this.codePush.sync().subscribe((syncStatus) => console.log(syncStatus));
      }

      this.tryLogin();
    });
  }

  hideSplashScreen()
  {
    if ( this.splashScreen ) {
       setTimeout(() => {
         this.splashScreen.hide();
       }, 500);
     }
  }

  tryLogin()
  {
    this.loginService.getCredentials().then((data) => {
      if ( data !== null )
      {
        var access_token = data;

        // Save access token in Constants.
        Constants.ACCESS_TOKEN = access_token;

        // Get user record.
        this.accountService.getUserRecord().subscribe((data) => {
          Constants.USER = data.json().data;

          // Set root page to dashboard.
          this.nav.setRoot(DashboardPage);

          // Publish login event
          this.events.publish('user:login');
        });

        // Get Region data
        this.accountService.getRegionData().subscribe((data) => {
          Constants.REGION_DATA = JSON.parse( data.json().data );
        });

        // Get all TNF user records
        this.accountService.getTnfs().subscribe((data) => {
          Constants.TNF_USERS = data.json().data;
        });
      } else {
        this.rootPage = LoginPage;
      }

    }, (error) => {
    });
  }

  activateMenu( menuId )
  {
    this.activeMenu = menuId;
    this.menu.enable(true, menuId);
  }

  toggleMenu()
  {
    this.menu.toggle();
  }

  watchNetwork()
  {
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      this.presentOfflineAlert();
    });

    let connectSubscription = this.network.onConnect().subscribe(() => {
      setTimeout(() => {
        // If we get a connection, exec the tryLogin() method.
        this.tryLogin();
      }, 3000);
    });
  }

  presentOfflineAlert()
  {
    let alert = this.alertCtrl.create({
      title: 'No internet connection',
      subTitle: 'Please connect to the internet to use the TNF app.',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  listenToEvents() {
    this.events.subscribe('activity:view', ( activity, activityId = null ) => {
      // If activity view page is already on the stack, don't push another
      // copy of it onto the stack.  Just update the data with an event.
      if ( this.nav.getActive().name === 'ActivityViewPage' )
      {
        this.events.publish( 'activity:updateview', activity );
      } else {
        this.nav.push(ActivityViewPage, { surveyAttempt: activity });
      }
    });

    // Log events
    this.events.subscribe('app:log', ( logData ) => {
      this.presentToast( logData );
    });

    this.events.subscribe('user:login', () => {
      this.activateMenu( 'activityMenu' );
    });

    this.events.subscribe('user:logout', () => {
      this.rootPage = LoginPage;
      Constants.REFRESH_DASHBOARD = true;
      this.activateMenu('blankMenu');
    });

    // Swap menus when viewing reports
    this.events.subscribe('reports:view', () => {
      this.activateMenu( 'reportMenu' );
    });

    // Swap menus when leaving reports
    this.events.subscribe('dashboard:view', () => {
      this.activateMenu( 'activityMenu' );
    });

    // Toggle left menu open and closed
    this.events.subscribe('menu:toggle', ( logData ) => {
      this.toggleMenu();
    });
  }

  presentToast( logData, duration = 3000, closeButtonText = 'OK' )
  {
    let toast = this.toastCtrl.create({
      message: logData.message,
      duration: duration,
      position: 'bottom',
      cssClass: 'toast-' + logData.status,
      showCloseButton: true,
      closeButtonText: closeButtonText
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }
}
