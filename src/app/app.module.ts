import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

// Ionic Native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Network } from '@ionic-native/network';
import { Keyboard } from '@ionic-native/keyboard';
import { DatePicker } from '@ionic-native/date-picker';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

// App component
import { TnfApp } from './app.component';

// Pages
import { CreateAccountPage } from '../pages/account/createaccount';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { SurveyPage } from '../pages/survey/survey';
import { ReportsPage } from '../pages/reports/reports';
import { ActivityViewPage } from '../pages/activity/activity-view';
import { UpdateAccountPage } from '../pages/account/updateaccount';
import { MapReportPage } from '../pages/reports/map-report';
import { FrequencyReportPage } from '../pages/reports/frequency-report';

// PDF Print Pages
import { ReportsFullReportPDFPage } from '../pages/pdf-creation/reports-full-report-pdf';
import { ActivityPDFPage } from '../pages/pdf-creation/activity-pdf';

// Modals and popovers
import { NotificationsNewModalPage } from '../components/modals/notifications-new-modal';
import { NotificationsViewAllModalPage } from '../components/modals/notifications-viewall-modal';
import { MapActivityPopoverPage } from '../components/popovers/map-activity-popover';
import { QuarterPopoverPage } from '../components/popovers/quarter-popover';
import { ActivityPopoverPage } from '../components/popovers/activity-popover';
import { ActivityModalPage } from '../components/modals/activity-modal';
import { GlobalUpdateModalPage } from '../components/modals/global-update-modal';

// Service providers
import { AccountService } from '../providers/account-service/account-service';
import { LoginService } from '../providers/login-service/login-service';
import { ReportService } from '../providers/report-service/report-service';
import { SurveyService } from '../providers/survey-service/survey-service';
import { PlacesService } from '../providers/places-service/places-service';
import { UserService } from '../providers/user-service/user-service';
import { AppDataService } from '../providers/app-data-service/app-data-service';
import { HelperService } from '../providers/helper-service/helper-service';
import { HelperPDFService } from '../providers/helper-service/helper-pdf-service';
import { ImageService } from '../providers/image-service/image-service';
import { ToDosService } from '../providers/todos-service/todos-service';
import { NotificationsService } from '../providers/notifications-service/notifications-service';
import { UiService } from '../providers/ui-service/ui-service';
import { GroupService } from '../providers/group-service/group-service';

// Other modules.
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';
import { LinkyModule } from 'angular-linky';

// Chart types
import { DonutComponent } from '../components/chart_types/donut';
import { BarComponent } from '../components/chart_types/bar';
import { BarSimpleComponent } from '../components/chart_types/bar-simple';
import { ActivityDurationReportComponent } from '../components/chart_types/activity-duration';
import { TextReportComponent } from '../components/chart_types/text';

// Question types
import {SurveyAttemptComponent} from '../pages/survey/question_types/survey-attempt';
import {HeadingComponent} from '../pages/survey/question_types/heading';
import {RadioComponent} from '../pages/survey/question_types/radio';
import {TextComponent} from '../pages/survey/question_types/text';
import {TextareafieldComponent} from '../pages/survey/question_types/textarea';
import {CheckboxComponent} from '../pages/survey/question_types/checkbox';
import {DatetimeComponent} from '../pages/survey/question_types/datetime';
import {DatepickerComponent} from '../pages/survey/question_types/datepicker';
import {SelectComponent} from '../pages/survey/question_types/select';
import {SelectmultiComponent} from '../pages/survey/question_types/selectmulti';
import {ListWithHeadersCheckboxComponent} from '../pages/survey/question_types/list-with-headers-checkbox';
import {ListWithHeadersTextComponent} from '../pages/survey/question_types/list-with-headers-text';
import {SelectCountyDistrictSchoolComponent} from '../pages/survey/question_types/select-county-district-school';

// Report components
import {ReportFilterControlComponent} from '../pages/reports/components/control.component';
import {ActivityBoxComponent} from '../components/activity-box.component';
import {ReportWidgetComponent} from '../pages/reports/components/report-widget.component';

// Activity list components
import { ActivityListComponent } from '../components/activity-list.component';

// ToDos components
import { ToDosComponent } from '../components/todos.component';
import { ToDosDashboardComponent } from '../components/todos-dashboard.component';

// Notifications dashboard components
import { NotificationsViewComponent } from '../components/notifications-view.component';
import { UpdatesViewComponent } from '../components/updates-view.component';

// Pipes
import { FilterPipeRegion } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeCounty } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeDistrict } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeSearch } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeActivityType } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeDeliverable } from '../pages/reports/pipes/activity-filter.pipes';
import { FilterPipeAudience } from '../pages/reports/pipes/activity-filter.pipes';

import { NewlinePipe } from '../app/pipes/common.pipes';
import { ReplacePipe } from '../app/pipes/common.pipes';

// Ionic native CodePush
import { CodePush } from '@ionic-native/code-push';

@NgModule({
  declarations: [
    TnfApp,
    CreateAccountPage,
    LoginPage,
    DashboardPage,
    SurveyPage,
    ReportsPage,
    ReportsFullReportPDFPage,
    ActivityPDFPage,
    ActivityViewPage,
    UpdateAccountPage,
    MapReportPage,
    FrequencyReportPage,

    SurveyAttemptComponent,
    HeadingComponent,
    RadioComponent,
    TextComponent,
    TextareafieldComponent,
    CheckboxComponent,
    DatetimeComponent,
    DatepickerComponent,
    SelectComponent,
    SelectmultiComponent,
    ListWithHeadersCheckboxComponent,
    ListWithHeadersTextComponent,
    SelectCountyDistrictSchoolComponent,
    TextReportComponent,

    ReportFilterControlComponent,
    ActivityBoxComponent,
    ActivityListComponent,
    ReportWidgetComponent,
    ToDosComponent,
    ToDosDashboardComponent,
    NotificationsViewComponent,
    UpdatesViewComponent,

    NotificationsNewModalPage,
    NotificationsViewAllModalPage,
    GlobalUpdateModalPage,
    ActivityModalPage,

    QuarterPopoverPage,
    MapActivityPopoverPage,
    ActivityPopoverPage,

    DonutComponent,
    BarComponent,
    BarSimpleComponent,
    ActivityDurationReportComponent,

    FilterPipeRegion,
    FilterPipeCounty,
    FilterPipeDistrict,
    FilterPipeSearch,
    FilterPipeActivityType,
    FilterPipeDeliverable,
    FilterPipeAudience,
    NewlinePipe,
    ReplacePipe
  ],
  imports: [
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(TnfApp, {
      backButtonText: '',
      backButtonIcon: 'arrow-back',
      platforms: {
        ios: {
          scrollAssist: false,
          autoFocusAssist: false
        }
      }
    }),
    HttpModule,
    BrowserModule,
    ChartsModule,
    FormsModule,
    LinkyModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TnfApp,
    CreateAccountPage,
    LoginPage,
    DashboardPage,
    MapReportPage,
    SurveyPage,
    ReportsPage,
    ReportsFullReportPDFPage,
    ActivityPDFPage,
    ActivityViewPage,
    NotificationsNewModalPage,
    NotificationsViewAllModalPage,
    QuarterPopoverPage,
    UpdateAccountPage,

    MapActivityPopoverPage,
    ActivityPopoverPage,

    ActivityModalPage,
    GlobalUpdateModalPage,
    FrequencyReportPage
  ],
  providers: [
    AccountService,
    LoginService,
    ReportService,
    SurveyService,
    PlacesService,
    UserService,
    AppDataService,
    HelperService,
    HelperPDFService,
    ToDosService,
    NotificationsService,
    UiService,
    ImageService,
    GroupService,

    FilterPipeRegion,
    FilterPipeCounty,
    FilterPipeDistrict,
    FilterPipeSearch,
    FilterPipeActivityType,
    FilterPipeDeliverable,
    FilterPipeAudience,
    NewlinePipe,
    ReplacePipe,

    StatusBar,
    SplashScreen,
    GoogleAnalytics,
    Keyboard,
    DatePicker,
    Network,
    File,
    FileOpener,
    CodePush,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
