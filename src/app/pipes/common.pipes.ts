import { Pipe, PipeTransform } from '@angular/core';
/*
 * Converts newlines into html breaks
*/
@Pipe({
  name: 'newline'
})

export class NewlinePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
      return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }
}

@Pipe({
  name: 'replace'
})

export class ReplacePipe implements PipeTransform {
  // value: string to modify
  // expr: string in which to search
  // replace: replacement string
  transform(value: string, expr: any, replace: string): any {
    if (!value)
      return value;

    return value.replace(new RegExp(expr, 'gi'), replace);
  }
}
