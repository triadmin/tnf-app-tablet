import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Pages
import { ReportsPage } from '../../pages/reports/reports';

// Services
import { ReportService } from '../../providers/report-service/report-service';
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';
import { HelperService } from '../../providers/helper-service/helper-service';

@Component({
  selector: 'bar-simple',
  template: `
    <div class="message" *ngIf="!haveData">
      <ion-row>
        <ion-col col-auto><ion-icon ios="ios-clipboard-outline" md="md-clipboard"></ion-icon></ion-col>
        <ion-col>You have no data for<br />{{ quarter.period }}.</ion-col>
      </ion-row>
    </div>
    <div *ngIf="haveData">
      <div *ngFor="let data of chartArray; let i = index">
        <span class="muted">{{ data.label }}</span>
        <div class="bar-simple-container" (click)="viewReports(this.reportType, data.label)">
          <div class="bar" [style.width]="data.percentage + '%'">{{ data.count }}</div>
        </div>
      </div>
    </div>
  `
})

export class BarSimpleComponent
{
  @Input() questionIdentifier: any;
  @Input() reportType: any;
  @Input() maxResults: number = 0;
  @Input() dateStart: any;
  @Input() dateEnd: any;
  @Input() dataElement: string = '';
  chartLabels: any;
  chartData: any;
  chartArray: any = [];
  haveData: boolean = true;
  quarter: any;
  filterObj: any = ReportDataStore.REPORT_FILTER_OBJECT;

  constructor(
    private nav: NavController,
    private reportService: ReportService,
    private helperService: HelperService
  ) {
    this.reportService = reportService;
  }

  getData( questionIdentifier )
  {
    this.reportService.getReportData( questionIdentifier, this.quarter ).subscribe(data => {
      this.prepDataForChart( data.json().data );
    });
  }

  prepDataForChart( data )
  {
    var chartLabels = [];
    var chartData = [];

    var chartObj = this.reportService.formatReportData( data, this.reportType, this.maxResults );

    if ( this.dataElement !== '' )
    {
      chartLabels = chartObj.chartLabels[ this.dataElement ];
      chartData = chartObj.chartData[ this.dataElement ];
    } else {
      chartLabels = chartObj.chartLabels;
      chartData = chartObj.chartData;
    }

    if ( chartData.length !== 0 )
    {
      this.chartArray = [];
      var chartArray = [];
      for ( var item in chartData )
      {
        var obj = {
          'label': chartLabels[item],
          'count': chartData[item]
        };
        chartArray.push( obj );
      }

      chartArray.sort(function(a, b){
        return b.count - a.count;
      });

      var maxValue = chartArray[0].count;
      for ( var item in chartArray )
      {
        var percentage = ( chartArray[item].count / maxValue ) * 100;
        var newObj = {
          'label': chartArray[item].label,
          'count': chartArray[item].count,
          'percentage': percentage
        };
        this.chartArray.push( newObj );
      }
    }

    if ( chartData.length === 0 )
    {
      this.haveData = false;
    } else {
      this.haveData = true;
    }
  }

  viewReports( reportType, label )
  {
    switch( reportType )
    {
      case 'Activity':
        this.filterObj.selectedActivities.length = 0;
        this.filterObj.selectedActivities.push( label );
        break;
      case 'Audience':
        this.filterObj.selectedAudiences.length = 0;
        this.filterObj.selectedAudiences.push( label );
        break;
      case 'Site':
        var locationObj = this.helperService.getRegionFromDistrictCounty( label );
        this.filterObj.selectedRegion = locationObj['region'];
        this.filterObj.selectedCounty = locationObj['county'];
        this.filterObj.selectedDistrict = locationObj['district'];
        break;
      default:
        break;
    }
    this.nav.push(ReportsPage);
  }

  // Reply on parent to tell us when to refresh the data.
  refreshChartData( quarter, data )
  {
    this.quarter = quarter;
    if ( data === null )
    {
      this.getData( this.questionIdentifier );
    } else {
      this.prepDataForChart( data );
    }
  }
}
