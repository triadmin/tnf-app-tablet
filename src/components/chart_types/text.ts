import { Component, Input } from '@angular/core';
import { ReportService } from '../../providers/report-service/report-service';

@Component({
  selector: 'textreport',
  template: `
    <h1 class="text-report-value" primary [innerHTML]="value"></h1>
  `,
  providers: [ReportService]
})

export class TextReportComponent
{
  @Input() questionIdentifier: any;
  @Input() reportType: any;
  @Input() dateStart: any;
  @Input() dateEnd: any;

  value: any;
  dateObj: any;

  constructor( private reportService: ReportService ) {
    this.reportService = reportService;
  }

  getData( questionIdentifier, reportType )
  {
    this.reportService.getReportData( questionIdentifier, this.dateObj ).subscribe(data => {
      var chartObj = this.reportService.formatReportData( data.json().data, reportType, null );
      this.value = ''; // Text goes here.
    });
  }

  // Reply on parent to tell us when to refresh the data.
  refreshChartData()
  {
    this.getData( this.questionIdentifier, this.reportType );
  }
}
