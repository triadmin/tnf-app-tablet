import { Component, Input, ViewChild } from '@angular/core';
import { ReportService } from '../../providers/report-service/report-service';
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'donut',
  template: `
    <div class="message" *ngIf="!haveData">
      <ion-row>
        <ion-col col-1><ion-icon ios="ios-clipboard-outline" md="md-clipboard"></ion-icon></ion-col>
        <ion-col>You have no data for<br />{{ quarter.period }}.</ion-col>
      </ion-row>
    </div>

    <canvas baseChart id="chart" #chart *ngIf="haveData" class="report-widget-doughnut-chart big"
      [data]="doughnutChartData"
      [labels]="doughnutChartLabels"
      [options]="doughnutChartOptions"
      [colors]="doughnutChartColors"
      [chartType]="chartType"></canvas>
    `
})

export class DonutComponent
{
  @Input() questionIdentifier: any;
  @Input() reportType: any;
  @Input() dateStart: any;
  @Input() dateEnd: any;

  // Get reference to chart
  @ViewChild('chart') chartDonut;

  // TODO: Figure out why labels aren't updating on the chart.
  // We shouldn't have to hard code them here like this.
  doughnutChartLabels: any = ['One time visits','Follow up visits'];
  doughnutChartColors: any = [
    { backgroundColor: ['#a1d489', '#b1e3f6'] }
  ];
  doughnutChartData: any = [];
  doughnutChartOptions: any = [];
  chartType: string = 'doughnut';
  haveData: boolean = true;
  quarter: any;

  constructor(
    private reportService: ReportService
  ) {
    this.reportService = reportService;

    this.doughnutChartOptions = {
      responsive: false,
      events: ['click'],
      legend: {
        display: false,
        fullWidth: true,
        position: 'bottom'
      }
    };
  }

  chartClicked(e) {
    //console.log(e);
  }

  chartHovered(e) {
    //console.log(e);
  }

  getData( questionIdentifier, reportType )
  {
    this.reportService.getReportData( questionIdentifier, this.quarter ).subscribe(data => {
      var chartObj = this.reportService.formatReportData( data.json().data, reportType, null );

      if ( typeof chartObj.chartLabels !== 'undefined' )
      {
        this.doughnutChartLabels = chartObj.chartLabels;
        this.doughnutChartData = chartObj.chartData;
      }

      if ( this.doughnutChartData.length === 0 )
      {
        this.haveData = false;
      } else {
        this.haveData = true;
      }
    });
  }

  getBase64Image()
  {
    var canvas = document.getElementById('chart') as HTMLCanvasElement;
    var dataURL = canvas.toDataURL();
    return dataURL;

    // For some reason this does not work.
    // Wonder what will happen when we have multiple charts on one screen.
    // Guess we'll find out.
    //var canvas = this.chartDonut as HTMLCanvasElement;

  }

  // Reply on parent to tell us when to refresh the data.
  refreshChartData( quarter )
  {
    this.quarter = quarter;
    this.getData( this.questionIdentifier, this.reportType );
  }
}
