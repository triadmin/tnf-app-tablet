import { Component } from '@angular/core';
import { Events, ModalController } from 'ionic-angular';

// Services
import { SurveyService } from '../providers/survey-service/survey-service';
import { NotificationsService } from '../providers/notifications-service/notifications-service';
import { HelperService } from '../providers/helper-service/helper-service';

// Modals
import { NotificationsViewAllModalPage } from '../components/modals/notifications-viewall-modal';

@Component({
  selector: 'notifications-view',
  templateUrl: 'notifications-view.component.html'
})
export class NotificationsViewComponent {
  notifications: any = [];
  showViewAll: boolean = true;

  constructor(
    private surveyService: SurveyService,
    private helperService: HelperService,
    private notificationsService: NotificationsService,
    private events: Events,
    public modalCtrl: ModalController
  ) {
  }

  markNotificationRead( notification )
  {
    notification.notification_read = 'Yes';
  
    this.notificationsService.markNotificationRead( notification.entryId ).subscribe(data => {
      notification.read_status = 'read';
    });
  }

  showActivityPage( activityId )
  {
    this.surveyService.getSurveyAttempt( activityId ).subscribe(data => {
      let activity = data.json().data;
      this.events.publish('activity:view', activity);
    });
  }

  presentNotificationsViewAllModal()
  {
    let notificationsViewAllModal = this.modalCtrl.create( NotificationsViewAllModalPage, {
    });
    notificationsViewAllModal.present();
  }

  refreshNotificationsList( readStatus = 'No' )
  {
    // getAllNotifications takes a read status of 'Yes' or 'No'
    this.notificationsService.getAllNotifications( readStatus ).subscribe(data => {
      // Sort notifications by date in DESC order.
      let notifications = data.json().data;
      notifications.sort((a, b) => {
        return new Date(b.dateSortable).getTime() - new Date(a.dateSortable).getTime();
      });

      this.notifications = notifications;
    });
  }
}  