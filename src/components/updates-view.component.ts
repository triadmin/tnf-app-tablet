import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { Constants } from '../providers/config/config';

// Services
import { HelperService } from '../providers/helper-service/helper-service';
import { GroupService } from '../providers/group-service/group-service';

// Modals
import { GlobalUpdateModalPage } from '../components/modals/global-update-modal';

@Component({
  selector: 'updates-view',
  template: `
    <div *ngIf="showUpdate" class="updates-container" (click)="presentUpdateModal()">
      <ion-icon name="ios-notifications-outline"></ion-icon>&nbsp;&nbsp;
      <strong> UPDATE:</strong> {{ post.ForumTitle }}
    </div>
  `
})
export class UpdatesViewComponent {
  showUpdate: boolean = false;
  post: any;

  constructor(
    private helperService: HelperService,
    private groupService: GroupService,
    private modalCtrl: ModalController
  ) {
    this.checkForUpdate();
  }

  checkForUpdate()
  {
    // Check for any new forum posts.
    this.groupService.getLastGroupForumPost().subscribe((data) => {
      this.post = data.json().data;

      // See if this post has already been viewed.
      this.groupService.getLastPostViewed( this.post.ForumId ).then((lastPostViewedId) => {
        if ( lastPostViewedId !== this.post.ForumId  )
        {
          // If post hasn't been viwed:
          // Mark the post as viewed,
          this.groupService.markPostAsViewed( this.post.ForumId );
          this.showUpdate = true;
        }
      });
    });
  }

  presentUpdateModal()
  {
    let post = this.post;
    let globalUpdateModal = this.modalCtrl.create( GlobalUpdateModalPage, {
      post
    });
    globalUpdateModal.present();
    // Turn off notification panel.
    this.showUpdate = false;
  }
}  