import { Component } from '@angular/core';
import { NavController, ViewController, NavParams, Events } from 'ionic-angular';

// Services
import { ReportDataStore } from '../../providers/report-service/report-datastore-service';

@Component({
  template: `
    <ion-list>
      <ion-list-header class="header-sm">Select Quarter</ion-list-header>
      <button ion-item *ngFor="let quarter of quarters" (click)='select(quarter)'>
        {{ quarter.period }}
      </button>
    </ion-list>
  `
})
export class QuarterPopoverPage {
  quarters: any = [];
  filterObj: any = ReportDataStore.REPORT_FILTER_OBJECT;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public events: Events
  )
  {
    // navParams contain our array of available dates.
    this.quarters = navParams.data.quarters;
  }

  select( quarter ) {
    ReportDataStore.REPORT_FILTER_OBJECT.selectedQuarter = quarter;
    ReportDataStore.REPORT_PERIOD = quarter.period;
    ReportDataStore.REPORT_FILTER_OBJECT.dataFetch = true;

    this.events.publish('quarters:select', quarter);
    this.viewCtrl.dismiss();
  }
}
