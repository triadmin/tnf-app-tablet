import { Component, ViewChild, ElementRef, Input } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { Constants } from '../../providers/config/config';

declare var window: any;

@Component({
  templateUrl: 'activity-modal.html'
})

export class ActivityModalPage
{
  selectedActivities: any = [];

  constructor(
    private viewCtrl: ViewController,
    private params: NavParams
  ) {
    this.viewCtrl = viewCtrl;
    this.selectedActivities = params.data.activities;
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }
}
