import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ViewController, Events, NavParams } from 'ionic-angular';

// Ionic Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';

// Pipes
import { NewlinePipe } from '../../app/pipes/common.pipes';

import { Constants } from '../../providers/config/config';

@Component({
  templateUrl: 'global-update-modal.html'
})

export class GlobalUpdateModalPage
{
  post: any;
  postDate: any;

  constructor(
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private events: Events,
    private ga: GoogleAnalytics
  ) {
    this.post = navParams.get('post');
    this.postDate = new Date(this.post.ForumCreatedAt);
    
    ga.trackView('View Global Update');
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }
}  