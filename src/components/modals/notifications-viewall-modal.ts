import { Component, ViewChild } from '@angular/core';
import { ViewController, Events } from 'ionic-angular';

// Ionic Native
import { GoogleAnalytics } from '@ionic-native/google-analytics';

import { Constants } from '../../providers/config/config';

// Components
import { NotificationsViewComponent } from '../../components/notifications-view.component';

@Component({
  templateUrl: 'notifications-viewall-modal.html'
})

export class NotificationsViewAllModalPage
{
  @ViewChild('notificationsmodal') notificationsModal: NotificationsViewComponent;

  constructor(
    private viewCtrl: ViewController,
    private events: Events,
    private ga: GoogleAnalytics
  ) {
    ga.trackView('View All Notifications');

    // TODO: Maybe figure out how to unsubscribe from 
    //       activity:view without unsubscribing everywhere :-|
    //this.events.unsubscribe('activity:view');

    // Listen for click event on activity links to close modal.
    this.events.subscribe('activity:view', () => {
      this.dismissModal();
    });
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }

  ionViewWillEnter()
  {
    // Get new notifications
    this.notificationsModal.refreshNotificationsList('all');
    // Turn off View All link
    this.notificationsModal.showViewAll = false;
  }
}  